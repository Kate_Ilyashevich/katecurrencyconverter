package com.example.kate.katecurrencyconverter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import Logic.Currency;
import Logic.LoadRateForDatTask;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.sql.Time;


/**
 * Created by kathe on 19.11.2016.
 */

public class ConverterActivity extends Activity{
    int myYear = 2016;
    int myMonth = 1;
    int myDay = 1;
    TextView dateTextView;
    CheckBox dateCheckBox;
    TextView fromTextView;
    TextView toTextView;
    EditText fromEditText;
    EditText toEditText;

    Boolean toByn = true;
    Spinner spinner;
    String todayString = "Today";
    String firstCurrString = "BYN";
    String secondCurrString;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);

        dateTextView = (TextView) findViewById(R.id.dateTextView);
        dateTextView.setText(todayString);
        fromEditText = (EditText) findViewById(R.id.fromEditText);
        toEditText = (EditText) findViewById(R.id.toEditText);
        fromTextView = (TextView) findViewById(R.id.fromTextView);
        toTextView = (TextView) findViewById(R.id.toTextView);
        dateCheckBox = (CheckBox) findViewById(R.id.dateCheckBox);
        toTextView.setText(firstCurrString);

        // адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Currency.currNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        // заголовок
        spinner.setPrompt("Title");
        // выделяем элемент
        spinner.setSelection(2);
        // устанавливаем обработчик нажатия
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
                secondCurrString = spinner.getSelectedItem().toString();
                if(toByn)
                    fromTextView.setText(secondCurrString);
                else
                    toTextView.setText(secondCurrString);
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    public void setDate(View v) {
        if ( !((CheckBox)v).isChecked() ) {
            new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay).show();
        }
        else {
            dateTextView.setText(todayString);
        }
    }

    public void swap(View v) {
        toByn = !toByn;
        CharSequence fromTextViewText = fromTextView.getText();
        CharSequence toTextViewText = toTextView.getText();
        toTextView.setText(fromTextViewText);
        fromTextView.setText(toTextViewText);
    }

    public void convert(View v)
    {
        LoadRateForDatTask task = new LoadRateForDatTask(this, this);
        String curName;
        if (toByn) {
            curName = fromTextView.getText().toString();
        }
        else{
            curName = toTextView.getText().toString();
        }

        if (dateCheckBox.isChecked() )
        {
            task.execute(curName, "");
        }
        else
        {
            String date = dateTextView.getText().toString();
            task.execute(curName, date);
        }
    }

    public void updateResult(Currency currency)
    {
        double sum = Double.parseDouble(fromEditText.getText().toString());
        double result;
        if (toByn) {
            result = sum * currency.getRate() / currency.getScale();
        }
        else
        {
            result = sum * currency.getScale() / currency.getRate();
        }
        toEditText.setText(String.valueOf(result));
    }

    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;
            dateTextView.setText(myYear + "-" + myMonth + "-" + myDay);
        }
    };
}
