package com.example.kate.katecurrencyconverter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Telephony;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Logic.Currency;
import Logic.CurrencyAdapter;
import Logic.DBHelper;
import Logic.LoadRatesTask;

/**
 * ExchangeRateActivity.
 */

public class ExchangeRateActivity extends Activity {
    ArrayList<Currency> currencies = new ArrayList<Currency>();
    ListView lvMain;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);

        CurrencyAdapter currencyAdapter;
        dbHelper = new DBHelper(getApplicationContext());


        if (isConnected()) {
            loadDataFromInternet();
        } else {
            Toast.makeText(this, "Check internet connection. There are probably old rates.",
                    Toast.LENGTH_LONG).show();
        }
        fillData(null);
        currencyAdapter = new CurrencyAdapter(this, currencies);

        lvMain = (ListView) findViewById(R.id.lvMain);
        lvMain.setAdapter(currencyAdapter);
    }
    public void fillData(List<Currency> currencyList)
    {
        if (currencyList == null) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor c = db.query(DBHelper.tableRatesName, null, null, null, null, null, null);

            if (c.moveToFirst()) {
                int idColumnIndex = c.getColumnIndex(DBHelper.idColumnName);
                int charCodeColumnIndex = c.getColumnIndex(DBHelper.charCodeColumnName);
                int nameColumnIndex = c.getColumnIndex(DBHelper.nameColumnName);
                int rateColumnIndex = c.getColumnIndex(DBHelper.rateColumnName);
                int scaleColumnIndex = c.getColumnIndex(DBHelper.scaleColumnName);

                do {
                    Currency currency = new Currency(c.getString(charCodeColumnIndex), c.getString(nameColumnIndex),
                            c.getDouble(rateColumnIndex), c.getInt(idColumnIndex), c.getInt(scaleColumnIndex));
                    currencies.add(currency);
                } while (c.moveToNext());
            }
            c.close();
        }
        else
        {
            currencies.clear();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            for (Currency currency : currencyList) {
                ContentValues cv = new ContentValues();
                cv.put(DBHelper.idColumnName, currency.getId());
                cv.put(DBHelper.charCodeColumnName, currency.getCharCode());
                cv.put(DBHelper.nameColumnName, currency.getName());
                cv.put(DBHelper.rateColumnName, currency.getRate());
                cv.put(DBHelper.scaleColumnName, currency.getScale());
                db.update(DBHelper.tableRatesName, cv, DBHelper.idColumnName + "= ?",
                        new String[]{Integer.toString(currency.getId())});
                currencies.add(currency);
            }
            dbHelper.close();
            CurrencyAdapter currencyAdapter;
            currencyAdapter = new CurrencyAdapter(this, currencies);
            lvMain.setAdapter(currencyAdapter);
        }
    }

    private void loadDataFromInternet()
    {
        LoadRatesTask ratesTask = new LoadRatesTask(this, this);
        ratesTask.execute(Currency.currNames);
    }

    private boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
