package com.example.kate.katecurrencyconverter;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;
import java.util.Random;

import Logic.Currency;
import Logic.DBHelper;
import Logic.LoadHistoryTask;

/**
 * Created by kathe on 24.12.2016.
 */

public class HistoryActivity extends Activity {

    String[] data = {"USD", "EUR", "RUB", "UAH"};
    GraphView graph;
    Spinner spinner;
    TextView tvScale;
    DBHelper dbHelper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        dbHelper = new DBHelper(getApplicationContext());
        // адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinner2);
        spinner.setAdapter(adapter);
        spinner.setSelection(2);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                updateCurrency();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        graph = (GraphView) findViewById(R.id.graph);
        tvScale = (TextView) findViewById(R.id.tvScale);
       updateCurrency();
    }

    private int getIdByCharCode(String charCode) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(DBHelper.tableRatesName, null, null, null, null, null, null);
        int id = 0;
        if (c.moveToFirst()) {
            int idColumnIndex = c.getColumnIndex(DBHelper.idColumnName);
            int charCodeColumnIndex = c.getColumnIndex(DBHelper.charCodeColumnName);
            int scaleColumnIndex = c.getColumnIndex(DBHelper.scaleColumnName);
            do {
                if(c.getString(charCodeColumnIndex).equals(charCode))
                {
                    id = c.getInt(idColumnIndex);
                    tvScale.setText("Scale 1:" + c.getInt(scaleColumnIndex));
                }
            } while (c.moveToNext());
        }
        c.close();
        return id;
    }
    private boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void updateCurrency(){
        if (isConnected()) {
            int id = getIdByCharCode(spinner.getSelectedItem().toString());
            LoadHistoryTask loadTask = new LoadHistoryTask(this, this);
            loadTask.execute(Integer.toString(id));

        } else {
            Toast.makeText(this, "Check internet connection. ",
                    Toast.LENGTH_LONG).show();
        }
    }
    public void updateGraph(List<Currency> currencies)
    {
        graph.removeAllSeries();
        DataPoint[] points = new DataPoint[currencies.size()];
        for(int i = 0; i < currencies.size(); i++)
            points[i] = new DataPoint(i, currencies.get(i).getRate());
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(points);
        series.setColor(Color.parseColor("#3f9742"));
        graph.addSeries(series);
    }
}
