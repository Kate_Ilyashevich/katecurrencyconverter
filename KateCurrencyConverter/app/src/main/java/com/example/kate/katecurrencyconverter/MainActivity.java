package com.example.kate.katecurrencyconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static final String ACTION_RATE_ACTIVITY = "com.example.kate.katecurrencyconverter.ExchangeRateActivity";
    public static final String ACTION_CONVERTER_ACTIVITY = "com.example.kate.katecurrencyconverter.ConverterActivity";
    public static final String ACTION_HISTORY_ACTIVITY = "com.example.kate.katecurrencyconverter.HistoryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onClick1(View view) {
        startActivity(new Intent(ACTION_RATE_ACTIVITY));
    }
    public void onClick2(View view) {
        startActivity(new Intent(ACTION_CONVERTER_ACTIVITY));
    }
    public void onClick3(View view) {
        startActivity(new Intent(ACTION_HISTORY_ACTIVITY));
    }
}
