package Logic;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.kate.katecurrencyconverter.HistoryActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kathe on 25.12.2016.
 */

public class LoadHistoryTask extends AsyncTask<String, Void, List<Currency>> {
    private static boolean firstTime = true;
    HistoryActivity historyActivity;
    private final Context context;
    private ProgressDialog progress;
    private static final String urlString =
            "http://www.nbrb.by/API/ExRates/Rates/Dynamics/%s?startDate=2016-10-5&endDate=2050-12-31";
    private static final String tag = "LoadHistoryTask";

    public LoadHistoryTask(Context c, HistoryActivity historyActivity) {
        this.context = c;
        this.historyActivity = historyActivity;
    }

    @Override
    protected void onPreExecute() {
        Log.d(tag, "onPreExecute");
        super.onPreExecute();
        progress = new ProgressDialog(this.context);
        progress.setMessage("Loading");
        progress.show();
    }

    @Override
    protected void onPostExecute(List<Currency> currencies) {
        Log.d(tag, "onPostExecute");
        super.onPostExecute(currencies);
        progress.dismiss();
        historyActivity.updateGraph(currencies);
    }

    @Override
    protected List<Currency> doInBackground(String... params) {
        Log.d(tag, "doInBackground");
        List<Currency> currencies = new LinkedList<>();
        for (String code : params) {
            List<Currency> nestCurrencies = loadDynamicsByCode(code);
            if (nestCurrencies != null) {
                currencies.addAll(nestCurrencies);
            }
        }
        return currencies;
    }

    public static List<Currency> loadDynamicsByCode(String id) {
        Log.d(tag, "Start load dynamic of currency.");
        try {
            URL url = new URL(String.format(urlString, id));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            int responseCode = connection.getResponseCode();
            final StringBuilder output = new StringBuilder();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    output.append(line);
                }
                br.close();
            } else {
                return null;
            }
            JSONArray currs = new JSONArray(output.toString());
            List<Currency> currencyList = new LinkedList<>();
            for (int i = 0; i < currs.length(); i++) {
                JSONObject curr = currs.getJSONObject(i);
                Currency currency = new Currency(curr.optDouble("Cur_OfficialRate"), curr.optInt("Cur_ID"));
                currencyList.add(currency);
            }
            return currencyList;
        } catch (Exception e) {
            return null;
        }
    }
}