package Logic;

import android.content.Context;
import android.icu.util.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kate.katecurrencyconverter.R;

import java.util.ArrayList;

/**
 * Created by kathe on 20.11.2016.
 */

public class CurrencyAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Currency> objects;

    public CurrencyAdapter(Context context, ArrayList<Currency> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.rowlayout, parent, false);
        }
        ImageView imageView = (ImageView)view.findViewById(R.id.icon);
        Currency c = getCurrency(position);
        switch (c.charCode){
            case "USD":
                imageView.setImageResource(R.drawable.united_states);
                break;
            case "EUR":
                imageView.setImageResource(R.drawable.european_union);
                break;
            case "RUB":
                imageView.setImageResource(R.drawable.russia);
                break;
            default:
                imageView.setImageResource(R.drawable.ukraine);
                break;
        }

        ((TextView) view.findViewById(R.id.currencyName)).setText(c.name );
        ((TextView) view.findViewById(R.id.currencyRate))
                .setText(c.scale + " " + c.charCode + " = " + Double.toString(c.rate) + " BYN");

        return view;
    }

    Currency getCurrency(int position) {
        return ((Currency) getItem(position));
    }

    ArrayList<Currency> getAll() {
        return objects;
    }
}
