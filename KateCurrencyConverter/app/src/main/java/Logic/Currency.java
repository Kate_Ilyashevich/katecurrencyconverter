package Logic;

/**
 * Created by kathe on 20.11.2016.
 */

public class Currency {
    public final static String[] currNames = {"USD", "EUR", "RUB", "UAH"};
    String charCode;
    String name;
    double rate;
    int scale;
    int id;

    public Currency(double _rate, int _id) {
        name = "";
        charCode = "";
        rate = _rate;
        id = _id;
        scale = -1;
    }
    public Currency(String _charCode, String _name, double _rate, int _id, int _scale) {
        name = _name;
        charCode = _charCode;
        rate = _rate;
        id = _id;
        scale = _scale;
    }
    public Currency(String _charCode, String _name, double _rate, int _id) {
        name = _name;
        charCode = _charCode;
        rate = _rate;
        id = _id;
        scale = 1;
    }
    public double getRate()
    {
        return rate;
    }

    public String getCharCode()
    {
        return charCode;
    }

    public String getName()
    {
        return name;
    }

    public int getScale()
    {
        return scale;
    }

    public int getId()
    {
        return id;
    }

}
