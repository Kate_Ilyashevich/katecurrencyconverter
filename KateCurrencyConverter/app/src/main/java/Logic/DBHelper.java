package Logic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

/**
 * Created by kathe on 24.12.2016.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static final String dbName = "currencyDB";
    public static final String tableRatesName = "rates";

    public static final String idColumnName = "id";
    public static final String charCodeColumnName = "charCode";
    public static final String nameColumnName = "name";
    public static final String rateColumnName = "rate";
    public static final String scaleColumnName = "scale";

    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, dbName, null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // создаем таблицу с полями
        db.execSQL("create table " + tableRatesName + " ("
                + idColumnName + " integer primary key, "
                + charCodeColumnName + " text, "
                + nameColumnName + " text, "
                + rateColumnName + " double, "
                + scaleColumnName + " integer);");

        // заполняем данными
        db.execSQL("insert into " + tableRatesName + " (id, charCode, name, rate, scale) " +
                "values (145, 'USD', 'Доллар США', 1.9572, 1);");
        db.execSQL("insert into " + tableRatesName + " (id, charCode, name, rate, scale) " +
                "values (292, 'EUR', 'Евро', 2.0733, 1);");
        db.execSQL("insert into " + tableRatesName + " (id, charCode, name, rate, scale) " +
                "values (298, 'RUB', 'Российский рубль', 3.0075, 100);");
        db.execSQL("insert into " + tableRatesName + " (id, charCode, name, rate, scale) " +
                "values (291, 'UAH', 'Гривен', 7.4224, 100);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion) {
    }
}
