package Logic;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.kate.katecurrencyconverter.ExchangeRateActivity;
import com.example.kate.katecurrencyconverter.HistoryActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kathe on 24.12.2016.
 */

public class LoadRatesTask extends AsyncTask<String, Void, List<Currency>> {
    private static boolean firstTime = true;
    ExchangeRateActivity activity;
    private final Context context;
    private ProgressDialog progress;
    private static final String urlString =
            "http://www.nbrb.by/API/ExRates/Rates/%s?ParamMode=2";
    private static final String tag = "LoadRatesTask";

    public LoadRatesTask(Context c, ExchangeRateActivity activity) {
        this.context = c;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        Log.d(tag, "onPreExecute");
        super.onPreExecute();
        progress = new ProgressDialog(this.context);
        progress.setMessage("Loading rates");
        progress.show();
    }

    @Override
    protected void onPostExecute(List<Currency> currencies) {
        Log.d(tag, "onPostExecute");
        super.onPostExecute(currencies);
        progress.dismiss();
        if (currencies != null)
            activity.fillData(currencies);
    }

    @Override
    protected List<Currency> doInBackground(String... params) {
        Log.d(tag, "doInBackground");
        List<Currency> currencies = new LinkedList<>();
        for (String code : params) {
            Currency currency = loadCurrentRateByCode(code);
            if (currency != null) {
                currencies.add(currency);
            }
            else
                return null;
        }
        return currencies;
    }

    public static Currency loadCurrentRateByCode(String code) {
        Log.d(tag, "Start loading current rate.");
        try {
            URL url = new URL(String.format(urlString, code));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            int responseCode = connection.getResponseCode();
            final StringBuilder output = new StringBuilder();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    output.append(line);
                }
                br.close();
            } else {
                return null;
            }
            JSONObject curr = new JSONObject(output.toString());
            Currency currency = null;
            currency = new Currency(curr.optString("Cur_Abbreviation"), curr.optString("Cur_Name"),
                    curr.optDouble("Cur_OfficialRate"), curr.optInt("Cur_ID"), curr.optInt("Cur_Scale"));
            return currency;
        } catch (Exception e) {
            return null;
        }
    }
}