package Logic;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.kate.katecurrencyconverter.ConverterActivity;
import com.example.kate.katecurrencyconverter.ExchangeRateActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by shilo on 26.12.2016.
 */

public class LoadRateForDatTask extends AsyncTask<String, Void, Currency> {
    private static boolean firstTime = true;
    ConverterActivity activity;
    private final Context context;
    private ProgressDialog progress;
    private static final String urlDateString =
            "http://www.nbrb.by/API/ExRates/Rates/%s?onDate=%s&ParamMode=2";
    private static final String urlNoDateString=
            "http://www.nbrb.by/API/ExRates/Rates/%s?ParamMode=2";
    private static final String tag = "LoadRatesTask";

    public LoadRateForDatTask(Context c, ConverterActivity activity) {
        this.context = c;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        Log.d(tag, "onPreExecute");
        super.onPreExecute();
        progress = new ProgressDialog(this.context);
        progress.setMessage("Loading rates");
        progress.show();
    }

    @Override
    protected void onPostExecute(Currency currency) {
        Log.d(tag, "onPostExecute");
        super.onPostExecute(currency);
        progress.dismiss();
        activity.updateResult(currency);
    }

    @Override
    protected Currency doInBackground(String... params) {
        Log.d(tag, "doInBackground");
        List<Currency> currencies = new LinkedList<>();
        if (params.length != 2)
            return null;
        Currency currency = loadCurrentRateByCode(params[0], params[1]);
        return currency;
    }

    public static Currency loadCurrentRateByCode(String code, String date) {
        Log.d(tag, "Start loading current rate.");
        try {
            URL url;
            if (date.equals(""))
                url = new URL(String.format(urlNoDateString, code));
            else
                url = new URL(String.format(urlDateString, date, code));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            int responseCode = connection.getResponseCode();
            final StringBuilder output = new StringBuilder();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    output.append(line);
                }
                br.close();
            } else {
                return null;
            }
            JSONObject curr = new JSONObject(output.toString());
            Currency currency = null;
            currency = new Currency(curr.optString("Cur_Abbreviation"), curr.optString("Cur_Name"),
                    curr.optDouble("Cur_OfficialRate"), curr.optInt("Cur_ID"), curr.optInt("Cur_Scale"));
            return currency;
        } catch (Exception e) {
            return null;
        }
    }
}
